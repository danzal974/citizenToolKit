<?php class AskDataAction extends CAction {
    
    public function run() {
        $controller = $this->getController();
        //$controller->layout = "//layouts/mainSearch";

        if(Yii::app()->request->isAjaxRequest)
			echo  $controller->renderPartial("askData");
        else 
			$controller->render( "askData");
    }
}