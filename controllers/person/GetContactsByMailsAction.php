
<?php
/**
* retreive dynamically 
*/
class GetContactsByMailsAction extends CAction
{
    public function run() {
    	try {
    		$res = (!empty($_POST["mailsList"] ) ? Element::getContactsByMails($_POST["mailsList"]) : array() );
    	} catch (Exception $e) {
    		$res = array("result" => false, "msg"=> $e->getMessage());
    	}
    	
		Rest::json($res);
		exit;
    }
}