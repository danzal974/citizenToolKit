<?php
class CommunityAction extends CAction
{
    public function run($id=null, $type=null)
    {
        $ctrl = $this->getController();
    	$this->getController()->layout = "//layouts/empty";

       // $form = PHDB::findOne( Form::COLLECTION , array("id"=>$id));

    	if ( ! Person::logguedAndValid() ) {
            $this->getController()->render("co2.views.default.unTpl",array("msg"=>Yii::t("common","Please Login First"),"icon"=>"fa-sign-in"));
        }else{
            //var_dump($id);exit;
            $community=Element::getCommunityByTypeAndId($type, $id, Link::ALL, Link::ALL_COMMUNITY);
            foreach ($community as $key => $value) {
                $elt=Element::getElementSimpleById($key, $value["type"], null, array("name", "profilThumbUrl", "tags","address", "email"));
                $community[$key]=array_merge($community[$key], $elt);
            }

			$this->getController()->renderPartial("community", array(
                "type"=>$type, 
                "id"=>$id, 
                "connectTo"=> Link::$linksTypes[$type][Person::COLLECTION], 
                "results" => $community));
		} 
    }
}