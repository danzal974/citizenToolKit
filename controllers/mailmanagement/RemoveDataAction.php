<?php

class RemoveDataAction extends CAction {
    
	public function run() {
		//$controller = $this->getController();
		$res = array("result" => false);
		try {
			

	       	$params = $_POST ;

	        if(Authorisation::verifCaptcha($params['captchaHash'], $params['captchaUserVal']) ){

	        	$valid = DataValidator::email( $params["email"] ) ;
				if( $valid  != "")
					throw new CTKException(Yii::t("common","The email is not well formated"));
				else{
					unset($params["captchaUserVal"]);
					unset($params["captchaHash"]);
					$params["date"] = time();
					PHDB::insert(Cron::ASK_COLLECTION, $params);
					$params["tpl"] ="removeData";
					$params["tplObject"]  = "[Communecter] ".Yii::t("common","Validation of actions");
					$params["from"] =Yii::app()->params['adminEmail'];
					$params["tplMail"]  = $params["email"];
					$params["active"] = (String) $params["_id"];
					Mail::createAndSend($params);
					$res["result"] = true;
					$res["msg"] = Yii::t("common","Request sent");
				}
			} else {
				$res["msg"] = Yii::t("common","Incorrect security code");
			}
		} catch (Exception $e) {
			$res["msg"] = $e->getMessage();
		}
		return Rest::json($res);
	}
}