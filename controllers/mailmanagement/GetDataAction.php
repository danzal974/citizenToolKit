<?php

class GetDataAction extends CAction {
    
	public function run($id = null, $email = null) {
		//$controller = $this->getController();
		try {
			if(!empty($id)){
				$res = Element::getDataByAsk($id);
			} else if(!empty($email) && Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) ) ) {
				$res = Element::getDataByAsk(null, $email);
			}

		} catch (Exception $e) {
			$res["msg"] = $e->getMessage();
		}
		return Rest::json($res);
	}
}